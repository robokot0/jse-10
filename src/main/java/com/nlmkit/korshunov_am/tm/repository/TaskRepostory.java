package com.nlmkit.korshunov_am.tm.repository;

import com.nlmkit.korshunov_am.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Репозитарий задач
 */
public class TaskRepostory {
    /**
     * Список задач
     */
    private List<Task> tasks = new ArrayList<>();

    /**
     * Создать задачу
     * @param name имя
     * @return задача
     */
    public Task create(String name) {
        final Task task = new Task(name);
        tasks.add(task);
        return task;
    }

    /**
     * Создать задачу
     * @param name имя
     * @param description описание
     * @return задача
     */
    public Task create(final String name,final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        tasks.add(task);
        return task;
    }

    /**
     * Изменить задачу
     * @param id идентификатор
     * @param name имя
     * @param description описание
     * @return задача
     */
    public Task update(final Long id,final String name,final String description) {
        final Task task = findById(id);
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    /**
     * Удалить все задачи.
     */
    public void clear() {
        tasks.clear();
    }

    /**
     * Найти задачу по индексу.
     * @param index Индекс
     * @return задача
     */
    public Task findByIndex(final int index){
        return  tasks.get(index);
    }

    /**
     * Найти задачу по имени.
     * @param name имя
     * @return задача
     */
    public Task findByName(final String name){
        for (final Task task: tasks) {
            if (task.getName().equals(name)) return task;
        }
        return null;
    }

    /**
     * Найти задачу по идентификатору.
     * @param id идентификатор
     * @return задача
     */
    public Task findById(final Long id){
        for (final Task task: tasks) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    /**
     * Ищем задачу по идентификатору проекта и по идентификатору задачи
     * @param projectId идентификатор проекта
     * @param id идентификатор задачи
     * @return задача
     */
    public Task findByProjectIdAndId(final Long projectId,final Long id){
        for (final Task task: tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }
    /**
     * Удалить задачу по индексу
     * @param index индекс
     * @return задача
     */
    public Task removeByIndex(final int index){
        final Task task = findByIndex(index);
        tasks.remove(task);
        return task;
    }

    /**
     * Удалить задачу по идентификатору
     * @param id идентификатор
     * @return задача
     */
    public Task removeById(final Long id){
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    /**
     * Удалить задачу по имени
     * @param name имя
     * @return задча
     */
    public Task removeByName(final String name){
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    /**
     * Найти по идентификатору проект и добавить к нему задачу
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @return задача null если задачи не найдено
     */
    public Task findAddByProjectId(final Long projectId,final Long taskId) {
        for (final Task task: findAll()) {
            if (taskId.equals(taskId)) {
                task.setProjectId(projectId);
                return task;
            }
        }
        return null;
    }

    /**
     * Получить список задач проекта
     * @param projectId идентрификатор проектв
     * @return списрк задач
     */
    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (projectId.equals(idProject)) result.add(task);
        }
        return result;
    }

    /**
     * Получить список всех задач
     * @return список задач
     */
    public List<Task> findAll() {
        return tasks;
    }

    /**
     * Получить количество задач
     * @return количество задач
     */
    public int size() {
        return tasks.size();
    }
}
