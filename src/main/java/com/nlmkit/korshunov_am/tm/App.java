package com.nlmkit.korshunov_am.tm;

import com.nlmkit.korshunov_am.tm.controller.ProjectController;
import com.nlmkit.korshunov_am.tm.controller.SystemController;
import com.nlmkit.korshunov_am.tm.controller.TaskController;
import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;
import com.nlmkit.korshunov_am.tm.service.ProjectService;
import com.nlmkit.korshunov_am.tm.service.ProjectTaskService;
import com.nlmkit.korshunov_am.tm.service.TaskService;

import java.util.Scanner;

import static com.nlmkit.korshunov_am.tm.TerminalConst.*;

/**
 *  Тестовое приложение
 */
public class App {
    /**
     * Репозитарий проектов.
     */
    private final ProjectRepository projectRepository = new ProjectRepository();
    /**
     * Репозитарий задач.
     */
    private final TaskRepostory taskRepostory = new TaskRepostory();
    /**
     * Сервис проектов.
     */
    private final ProjectService projectService = new ProjectService(projectRepository);
    /**
     * Сервис задач.
     */
    private final TaskService taskService = new TaskService(taskRepostory);
    /**
     * Сервис задачи в проекте
     */
    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository,taskRepostory);
    /**
     * Контролелер проектов.
     */
    private final ProjectController projectController = new ProjectController(projectService);
    /**
     * Контролер задач.
     */
    private final TaskController taskController = new TaskController(taskService, projectTaskService);
    /**
     * Контроллер системных комманд.
     */
    private final SystemController systemController = new SystemController();

    /**
     * Тестовые данные.
     */
    {
        projectRepository.create("P1");
        projectRepository.create("P2");

        taskRepostory.create("T1");
        taskRepostory.create("T2");

    }

    /**
     * Точка входа
     * @param args Аргументы коммандной строки.
     */
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final App app = new App();
        app.run(args);
        app.systemController.displayWelcome();
        String command = "";
        while (!(EXIT.equals(command) || SHORT_EXIT.equals(command))) {
            command = scanner.nextLine();
            app.run(command);
        }
    }

    /**
     * Выполнение комманды из аргументов командной строки вызова программы
     * @param args Аргументы коммандной строки.
     */
    public void run(final String[] args) {
        if (args == null) return;
        if (args.length <1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    /**
     * Полчить ссылку на сервис проектов
     * @return ссылка на сервис проектов
     */
    public ProjectService getProjectService() {
        return projectService;
    }

    /**
     * Получить ссылку на сервис задач
     * @return ссылка на сервис задач
     */
    public TaskService getTaskService() {
        return taskService;
    }

    /**
     * Получить ссылку на сервис задач и проектов
     * @return ссылка на сервис задач и проектов
     */
    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    /**
     * Выполнение комманды
     * @param param Команда
     * @return -1 ошибка 0 Выполнено
     */
    public int run(final String param) {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case SHORT_VERSION:
            case VERSION: return systemController.displayVersion();
            case SHORT_ABOUT:
            case ABOUT: return systemController.displayAbout();
            case SHORT_HELP:
            case HELP: return systemController.displayHelp();
            case SHORT_EXIT:
            case EXIT: return systemController.displayExit();

            case SHORT_PROJECT_CREATE:
            case PROJECT_CREATE: return projectController.createProject();
            case SHORT_PROJECT_CLEAR:
            case PROJECT_CLEAR: return projectController.clearProject();
            case SHORT_PROJECT_LIST:
            case PROJECT_LIST: return projectController.listProject();
            case SHORT_PROJECT_VIEW:
            case PROJECT_VIEW: return projectController.viewProjectByIndex();
            case SHORT_PROJECT_REMOVE_BY_ID:
            case PROJECT_REMOVE_BY_ID:return projectController.removeProjectByID();
            case SHORT_PROJECT_REMOVE_BY_NAME:
            case PROJECT_REMOVE_BY_NAME:return projectController.removeProjectByName();
            case SHORT_PROJECT_REMOVE_BY_INDEX:
            case PROJECT_REMOVE_BY_INDEX:return projectController.removeProjectByIndex();
            case SHORT_PROJECT_UPDATE_BY_INDEX:
            case PROJECT_UPDATE_BY_INDEX:return projectController.updateProjectByIndex();

            case SHORT_TASK_CREATE:
            case TASK_CREATE: return taskController.createTask();
            case SHORT_TASK_CLEAR:
            case TASK_CLEAR: return taskController.clearTask();
            case SHORT_TASK_LIST:
            case TASK_LIST: return taskController.listTask();
            case SHORT_TASK_VIEW:
            case TASK_VIEW: return taskController.viewTaskByIndex();
            case SHORT_TASK_REMOVE_BY_ID:
            case TASK_REMOVE_BY_ID:return taskController.removeTaskByID();
            case SHORT_TASK_REMOVE_BY_NAME:
            case TASK_REMOVE_BY_NAME:return taskController.removeTaskByName();
            case SHORT_TASK_REMOVE_BY_INDEX:
            case TASK_REMOVE_BY_INDEX:return taskController.removeTaskByIndex();
            case SHORT_TASK_UPDATE_BY_INDEX:
            case TASK_UPDATE_BY_INDEX:return taskController.updateTaskByIndex();
            case SHORT_TASK_ADD_TO_PROJECT_BY_IDS:
            case TASK_ADD_TO_PROJECT_BY_IDS:return taskController.addTaskToProjectByIds();
            case SHORT_TASK_REMOVE_FROM_PROJECT_BY_IDS:
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:return taskController.removeTaskFromProjectByIds();
            case SHORT_TASK_LISTS_BY_PROJECT_ID:
            case TASK_LISTS_BY_PROJECT_ID:return taskController.listTaskByProjectId();
            default:return systemController.displayError();
        }

    }


}
