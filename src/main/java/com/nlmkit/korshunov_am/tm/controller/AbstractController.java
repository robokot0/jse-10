package com.nlmkit.korshunov_am.tm.controller;

import java.util.Scanner;

/**
 * Базовый абстрактный класс для контроллеров
 */
public abstract class AbstractController {
    /**
     * объект для ввода комманд
     */
    protected final Scanner scanner = new Scanner(System.in);
}
