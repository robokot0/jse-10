package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;
import com.nlmkit.korshunov_am.tm.entity.Project;

import java.util.List;

/**
 * Сервис проектов
 */
public class ProjectService {
    /**
     * Репозитарий проектов.
     */
    private final ProjectRepository projectRepository;

    /**
     * Конструктор
     * @param projectRepository Репозитарий проектов.
     */
    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    /**
     * Создать проект
     * @param name имя
     * @return проект
     */
    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    /**
     * Создать проект
     * @param name имя
     * @param description описание
     * @return проект
     */
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    /**
     * Изменить проект
     * @param id идентификатор
     * @param name имя
     * @param description описание
     * @return проект
     */
    public Project update(final Long id, final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.update(id, name, description);
    }

    /**
     * Удалить все проекты
     */
    public void clear() {
        projectRepository.clear();
    }

    /**
     * Найти проект по индексу
     * @param index индекс
     * @return проект
     */
    public Project findByIndex(final int index) {
        if (index < 0 || index >= projectRepository.size()) return null;
        return projectRepository.findByIndex(index);
    }

    /**
     * Найти проект по итмени
     * @param name имя
     * @return проект
     */
    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    /**
     * Найти проект про идентификатору
     * @param id идентификатор
     * @return проект
     */
    public Project findById(final Long id) {
        if (id == null) return null;
        return projectRepository.findById(id);
    }

    /**
     * Удалитьпроект по индексу
     * @param index индекс
     * @return проект
     */
    public Project removeByIndex(final int index) {
        if (index < 0 || index >= projectRepository.size()) return null;
        return projectRepository.removeByIndex(index);
    }

    /**
     * Удалить проект по идентификатору
     * @param id идентификатор
     * @return проект
     */
    public Project removeById(final Long id) {
        if (id == null) return null;
        return projectRepository.removeById(id);
    }

    /**
     * Удалить проект по имени
     * @param name имя
     * @return проект
     */
    public Project removeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }

    /**
     * Получить все проекты
     * @return список проектов
     */
    public List<Project> findAll() {
        return projectRepository.findAll();
    }
}
